package com.ap.cerebrum.util;

/**
 * Created by aniket on 2/1/14.
 */
import android.content.Context;
import android.content.Intent;

public final class GlobalConstants {

    public static final String TAG = "com.ap.Cerebrum";

    public static final String SERVER_URL = "http://www.aniketpansare.com";//http://192.168.0.37/
    public static final String SEND_MESSAGE_URL =SERVER_URL+"/cerebrum/send_message.php";
    public static final String DATABASE_REGISTER_URL=SERVER_URL+"/cerebrum/register.php";
    public static final String DATABASE_GET_CONTACTS_URL=SERVER_URL+"/cerebrum/get_contacts.php";

    // Google project id
    public static final String SENDER_ID = "269142109448";

    //Shared Preferences File name and keys
    public static String PREF_FILE_NAME = "REGISTRATION_INFO";
    public static String REGISTRATION_ID = "registrationID";
    public static String NAME = "name";
    public static String EMAIL = "email";
    public static String PROPERTY_APP_VERSION = "appVersion";

    public static String CONTACT_FILE_NAME = "CONTACT_INFO";

    public static final String DISPLAY_MESSAGE_ACTION =
            "com.ap.cerebrum.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";

    /// Minimum length of a username.
    public static final int USERNAME_MIN_LENGTH = 4;

    /// Maximum length of a username.
    public static final int USERNAME_MAX_LENGTH = 64;


    /// Minimum length of a password.
    public static final int PASSWORD_MIN_LENGTH = 8;


    /// Maximum length of a password
    public static final int PASSWORD_MAX_LENGTH = 512;

    /// Maximum length of an email address.
    public static final int EMAIL_MAX_LENGTH = 128;


    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}