package com.ap.cerebrum.util;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ap.cerebrum.R;

import java.util.List;

/**
 * Created by aniket on 3/20/14.
 */
public class ContactListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final List<String> values;

    /**
     * Constructor.
     * @param context
     * @param values
     */
    public ContactListAdapter(Context context,List<String> values)
    {
        super(context, R.layout.contactlist_row_layout,values);
        this.context=context;
        this.values=values;
    }

    /**
     * Override getView Method.
     * Called when array adapter initialized.
     * Called once for each element in the list.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.contactlist_row_layout,parent,false);

        TextView textView = (TextView) rowView.findViewById(R.id.contact_name);


        if(values.get(position).split(":")[0].contentEquals("1"))
        {
            textView.setText(values.get(position).replaceFirst("1:",""));
            textView.setTextColor(Color.BLACK);
        }
        else //Received Messages
        {
            textView.setText(values.get(position).replaceFirst("0:", ""));

        }
        return rowView;
    }
}
