package com.ap.cerebrum.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ap.cerebrum.R;
import com.google.android.gms.drive.internal.p;

import java.util.List;

/**
 * Array Adapter to display chat history.
 * Chat bubbles displayed based on message type (Sent or Received).
 * @author Aniket Pansare
 */
public class ChatArrayAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> values;

    /**
     * Constructor.
     * @param context
     * @param values
     */
    public ChatArrayAdapter(Context context,List<String> values)
    {
        super(context, R.layout.chat_row_layout,values);
        this.context=context;
        this.values=values;
    }

    /**
     * Override getView Method.
     * Called when array adapter initialized.
     * Called once for each element in the list.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.chat_row_layout,parent,false);
        LinearLayout layoutLeft= (LinearLayout) rowView.findViewById(R.id.linear_layout_left);
        LinearLayout layoutRight= (LinearLayout) rowView.findViewById(R.id.linear_layout_right);
        TextView textViewRight = (TextView) rowView.findViewById(R.id.message_text);
        TextView textViewLeft = (TextView) rowView.findViewById(R.id.message_text_left);
        TextView textViewLeftName = (TextView) rowView.findViewById(R.id.name_left);

        //Sent Messages
        if(values.get(position).split(":")[0].contentEquals("0"))
        {
            textViewRight.setText(values.get(position).replaceFirst("0:",""));
            textViewLeft.setVisibility(View.INVISIBLE);
            layoutLeft.setVisibility(View.INVISIBLE);
        }
        else //Received Messages
        {
            textViewLeftName.setText(values.get(position).split(":")[0]);
            textViewLeft.setText(values.get(position).replaceFirst("^.*1:",""));
            textViewRight.setVisibility(View.INVISIBLE);
            layoutRight.setVisibility(View.INVISIBLE);
        }
        return rowView;
    }
}
