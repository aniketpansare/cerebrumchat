package com.ap.cerebrum;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ap.cerebrum.database.ChatDataSource;
import com.ap.cerebrum.domain.Contact;
import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.ContactManager;
import com.ap.cerebrum.service.GCMManager;
import com.ap.cerebrum.service.Impl.ChatManagerImpl;
import com.ap.cerebrum.service.Impl.GCMManagerImpl;
import com.ap.cerebrum.util.ChatArrayAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Chat Window Activity.
 * Displays Chat Messages between two users.
 * Interface for sending and receiving messages.
 * @author Aniket Pansare
 */
public class ChatWindowActivity extends ListActivity {

    static final String TAG = "com.ap.cerebrum.ChatWindowActivity";
    private ChatManager chatManager = null;
    private GCMManager gcmManager = null;
    TextView message_text;
    TextView messageToSend;
    Context context;
    String chatWindowName="";
    List<String> chatHistory = new ArrayList<String>();
    ChatArrayAdapter listAdapter;
    private ChatDataSource dataSource;
    private BroadcastReceiver broadcastReceiver = new CerebrumBroadcastReceiverInner();
    public static boolean isActivityVisible=false;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chatwindow);
        context = getApplicationContext();
        chatManager = new ChatManagerImpl();
        gcmManager = new GCMManagerImpl();

        //set visibility to true.
        isActivityVisible=true;

        message_text = (TextView) findViewById(R.id.message_text);
        messageToSend = (TextView) findViewById(R.id.messageToSend);

        //Sets Action Bar Title
        chatWindowName= getIntent().getExtras().getString("name");
        setTitle(chatWindowName);

        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (gcmManager.checkPlayServices(context,this)) {
            gcmManager.registerInBackground(context);
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
            finish();
        }

        //open database and fetch chat history.
        dataSource= new ChatDataSource(this);
        chatHistory = dataSource.getChatHistory(chatWindowName);

        //Initialize Custom Array Adapter with chat history.
        listAdapter = new ChatArrayAdapter(context,chatHistory);
        setListAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK. If check fails stop the app.
        if(!gcmManager.checkPlayServices(context,this))
        {
            finish();
        }

        //set visibility to true.
        isActivityVisible=true;

        chatHistory = dataSource.getChatHistory(chatWindowName);
        //Initialize Custom Array Adapter with chat history.
        listAdapter = new ChatArrayAdapter(context,chatHistory);
        setListAdapter(listAdapter);

        /**************************************
         * Register Broadcast receiver
         ***************************************/
         registerReceiver(broadcastReceiver, new IntentFilter("com.google.android.c2dm.intent.RECEIVE"));

    }

    // Send an upstream message to another device.
    public void onClick(final View view) {

        if (view == findViewById(R.id.sendButton)) {
            //Start asynchronous task to send message to 3rd party server.
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = ((EditText)findViewById(R.id.messageToSend)).getText().toString();
                    //Send message to contact in contact list.. currently specified null and hard coded.
                    ChatManager chatManager = new ChatManagerImpl();
                    Contact contact = ContactManager.createContact(chatWindowName, "DummyEmail.com",
                            chatManager.getContactRegId(getApplicationContext(),chatWindowName));
                    return chatManager.sendMessage(contact,msg,context);
                }

                @Override
                protected void onPostExecute(String msg) {
                    dataSource.createChatData(chatWindowName, msg, 0);
                    //chatHistory.add("0:"+msg);
                    //listAdapter.notifyDataSetChanged();
                    chatHistory = dataSource.getChatHistory(chatWindowName);
                    listAdapter = new ChatArrayAdapter(context,chatHistory);
                    setListAdapter(listAdapter);

                    messageToSend.setText("");
                }
            }.execute(null, null, null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //set visibility to false.
        isActivityVisible=false;
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        /**************************************
         * Register Broadcast receiver
         ***************************************/
        unregisterReceiver(broadcastReceiver);

        //set visibility to false.
        isActivityVisible=false;
    }


    private class CerebrumBroadcastReceiverInner extends BroadcastReceiver {

        static final String TAG = "com.ap.cerebrum.CerebrumBroadCastReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {

            //ChatManager chatManager = new ChatManagerImpl();
            String action = intent.getAction();

            //Handle the received intent.
            if(action.equals("com.google.android.c2dm.intent.REGISTRATION")){
               // chatManager.handleRegistration(context,intent);
            } else if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
                //chatManager.handleMessage(context,intent);
                if(intent.getStringExtra("chatText")!=null)
                {
                    chatHistory.add(chatWindowName+":1:"+intent.getStringExtra("chatText"));
                    listAdapter = new ChatArrayAdapter(context,chatHistory);
                    setListAdapter(listAdapter);
                }
            }
        }

    }

}
