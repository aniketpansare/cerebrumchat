package com.ap.cerebrum.domain;

import com.ap.cerebrum.domain.Impl.ContactImpl;

import java.io.Serializable;

/**
 * Contact domain class.
 * @author Aniket Pansare
 */
public abstract class Contact implements Serializable {

    /******************************
     * Static factory Methods.
     ******************************/

    /**
     * Default Constructor
     * @return
     */
    public static Contact create()
    {
        return new ContactImpl();
    }

    /**
     * Full Constructor
     * @param name
     * @param email
     * @param registrationId
     * @return
     */
    public static Contact create(String name,String email, String registrationId)
    {
        return new ContactImpl(name,email,registrationId);
    }


    /*****************************************************************************
     *                  Interface Methods
     *************************************************************************/

    public abstract String getName();

    public abstract void setName(String name);

    public abstract String getEmail();

    public abstract void setEmail(String email);

    public abstract String getRegistrationId();

    public abstract void setRegistrationId(String registrationId);

    public abstract String getContactNumber();

    public abstract void setContactNumber(String contactNumber);
}
