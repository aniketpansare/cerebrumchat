package com.ap.cerebrum.domain.Impl;

/**
 * Created by aniket on 3/17/14.
 */
public class ChatDataImpl {
    private long id;
    private String name;
    private String chattext;
    private boolean direction; //true= from, false=to

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChattext() {
        return chattext;
    }

    public void setChattext(String chattext) {
        this.chattext = chattext;
    }

    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    @Override
    public String toString()
    {
        return chattext;
    }
}
