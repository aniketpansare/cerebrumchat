package com.ap.cerebrum.domain;

/**
 * Stores chat information.
 * @author Aniket Pansare
 */
public interface ChatData {

    /******************************************************************************
     * Interface Methods.
     ******************************************************************************/
    public long getId();

    public void setId(long id);

    public String getName();

    public void setName(String name);

    public String getChattext();

    public void setChattext(String chattext);

    public boolean isDirection();

    public void setDirection(boolean direction);

    @Override
    public String toString();

}
