package com.ap.cerebrum.domain.Impl;

import com.ap.cerebrum.domain.Contact;

/**
 * Created by aniket on 3/14/14.
 */
public class ContactImpl extends Contact {

    /**********************************************************************************/
	/* Members */
    /**********************************************************************************/

    /**
     * The name of the user.
     */
    private String name;

   /**
     * The user's email address.
     */
    private String email;


    /**
     * The device registration id.
     */
    private String registrationId;

    /**
     * The user's phone number.
     */
    private String contactNumber;

    /**
     * Default Constructor.
     */
    public ContactImpl()
    {
        name="";
        email="";
        contactNumber="";
        registrationId="";
    }

    public ContactImpl(String name,String email, String registrationId)
    {
        if(name==null || email==null) throw new NullPointerException();
        this.name=name;
        this.email=email;
        this.registrationId=registrationId;
    }


    /******************* Getter & Setter **********************/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name==null) throw new NullPointerException();
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(email==null) throw new NullPointerException();
        this.email = email;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        if(registrationId==null) throw new NullPointerException();
        this.registrationId = registrationId;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        if(contactNumber==null) throw new NullPointerException();
        this.contactNumber = contactNumber;
    }
}



