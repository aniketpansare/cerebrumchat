package com.ap.cerebrum;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.Impl.ChatManagerImpl;

/**
 * Broadcast Receiver.
 * Receives intents from GCM server.
 * Handles both Registration and Receive Intent.
 * @author Aniket Pansare
 */
public class CerebrumBroadcastReceiver extends BroadcastReceiver {

    static final String TAG = "com.ap.cerebrum.CerebrumBroadCastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        ChatManager chatManager = new ChatManagerImpl();
        String action = intent.getAction();

        //Handle the received intent.
        if(action.equals("com.google.android.c2dm.intent.REGISTRATION")){
            chatManager.handleRegistration(context,intent);
        } else if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
            chatManager.handleMessage(context,intent);

            if(intent.getStringExtra("chatText")!=null) {
                    Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(1000);


                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(context)
                                    .setSmallIcon(R.drawable.ic_launcher)
                                    .setContentTitle("New Message")
                                    .setContentText(intent.getStringExtra("fromName"));
                    // Creates an explicit intent for an Activity in your app
                    Intent resultIntent = new Intent(context, RegisterActivity.class);
                    resultIntent.putExtra("bName",intent.getStringExtra("fromName"));


                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                    // Adds the back stack for the Intent (but not the Intent itself)
                    stackBuilder.addParentStack(RegisterActivity.class);
                    // Adds the Intent that starts the Activity to the top of the stack
                    stackBuilder.addNextIntent(resultIntent);
                    PendingIntent resultPendingIntent =
                            stackBuilder.getPendingIntent(
                                    0,
                                    PendingIntent.FLAG_UPDATE_CURRENT
                            );
                    mBuilder.setContentIntent(resultPendingIntent);
                    NotificationManager mNotificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    // mId allows you to update the notification later on.
                    int notificationId = 10;
                    mNotificationManager.notify(notificationId, mBuilder.build());
            }
        }
    }

}