package com.ap.cerebrum.tasks;

import android.os.AsyncTask;

import com.ap.cerebrum.util.GlobalConstants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aniket on 3/16/14.
 */
public class SendMessageTask  extends AsyncTask<String, Void, Void>
{

    @Override
    protected Void doInBackground(String... params) {

        try{
            //Connect to the server
            URL url = new URL(GlobalConstants.SEND_MESSAGE_URL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");

            //Send parameters and registraion id to server.
            httpURLConnection.setDoOutput(true);
            String parameters = "toRegId="+params[0]+"&fromRegId="+params[1]+"&message="+params[2]+
                    "&toEmail="+params[3]+"&fromEmail="+params[4]+"&toName="+params[5]+"&fromName="+params[6];
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.writeBytes(parameters);
            dataOutputStream.flush();
            dataOutputStream.close();

            //Get response Code
            int responseCode = httpURLConnection.getResponseCode();
            //Get response
            BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuffer response = new StringBuffer();
            String str;
            while((str= br.readLine()) != null)
            {
                response.append(str);
            }
            br.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getStackTrace());
        }
        return null;
    }
}

