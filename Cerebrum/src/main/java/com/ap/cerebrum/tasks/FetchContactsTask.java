package com.ap.cerebrum.tasks;

import android.os.AsyncTask;

import com.ap.cerebrum.ContactListActivity;
import com.ap.cerebrum.util.GlobalConstants;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by aniket on 3/15/14.
 */
//AsyncTask<Type of parameters, Type of Progress Units published, Type of result>
public class FetchContactsTask extends AsyncTask<String, Void, Void>
{

    private ContactListActivity activity;

    public FetchContactsTask(ContactListActivity a)
    {
        this.activity = a;
    }


    @Override
    protected Void doInBackground(String... params) {

        try{
            //Connect to the server
            URL url = new URL(GlobalConstants.DATABASE_GET_CONTACTS_URL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");

            //Send parameters and registraion id to server.
            httpURLConnection.setDoOutput(true);
            String parameters = "regId="+params[0]+"&name="+params[1]+"&email="+params[2];
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.writeBytes(parameters);
            dataOutputStream.flush();
            dataOutputStream.close();

            //Get response Code
            int responseCode = httpURLConnection.getResponseCode();
            //Get response
            BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuffer response = new StringBuffer();
            String str;
            while((str= br.readLine()) != null)
            {
                response.append(str);
            }
            br.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getStackTrace());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        activity.fetchContactsCallback();
    }

}