package com.ap.cerebrum.service;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.IOException;

/**
 * Created by aniket on 3/14/14.
 * @author Aniket Pansare
 */
public interface GCMManager {

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     * @param context
     * @param activity
     * @return
     */
    public boolean checkPlayServices(Context context, Activity activity);

    /**
     * Registers the application with GCM servers asynchronously.
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     * @param context
     */
     public void registerInBackground(Context context);

    /**
     * Sends bundled data to GCM server.
     * @param data
     * @param context
     * @throws IOException
     */
    public void sendMessage(Bundle data,Context context) throws IOException;
}
