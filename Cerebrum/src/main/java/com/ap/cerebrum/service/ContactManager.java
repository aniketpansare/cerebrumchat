package com.ap.cerebrum.service;

import com.ap.cerebrum.domain.Contact;

import java.util.List;

/**
 * Created by aniket on 3/14/14.
 * @author Aniket Pansare
 */
public abstract class ContactManager {

    /**
     * Fetches the contact list for the user.
     * @return List of Contacts
     */
    public  abstract List<Contact> getContacts();

    /**
     * Creates and returns contact object.
     * @param name
     * @param email
     * @param registrationId
     * @return
     */
    public static Contact createContact(String name, String email, String registrationId) {
        return Contact.create(name,email,registrationId);
    }

    /**
     * Removes contact from contact list.
     * @param contact
     */
    public abstract void removeContact(Contact contact);


}
