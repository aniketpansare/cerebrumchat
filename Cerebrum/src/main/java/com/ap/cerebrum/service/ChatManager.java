package com.ap.cerebrum.service;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.ap.cerebrum.domain.Contact;

import java.util.List;
import java.util.Set;

/**
 * Created by aniket on 3/14/14.
 * @author Aniket Pansare
 */
public interface ChatManager {

    /**
     * The registration id given by GCM server is stored in the chat Server.
     * @param context
     * @param intent
     */
    public void handleRegistration(Context context, Intent intent);

    /**
     * The message received from GCM server is handled.
     * @param context
     * @param intent
     */
    public void handleMessage(Context context, Intent intent);


    /**
     * Sends message to GCM server.
     * @param toContact
     * @param msg
     * @param context
     * @return
     */
    public String sendMessage(Contact toContact,String msg,Context context);

    void storeContactList(Context context, List<Contact> contacts);

    void storeName(Context context, String name);

    void storeEmail(Context context, String email);

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    public void storeRegistrationId(Context context, String regId);

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public String getRegistrationId(Context context);

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    Set<String> getContactNames(Context context);

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    String getContactRegId(Context context, String contactName);

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    String getNameFromPreferences(Context context);

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    String getEmailFromPreferences(Context context);

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public int getAppVersion(Context context);

    /**
     * Checks if the user is Logged in.
     * @return
     */
    public boolean isLoggedIn(Context context);

    /**
     * Activate Application.. Register device with GCM.
     * @param context
     * @param name
     * @param email
     */
    public void activateApp(Context context, String name, String email);
}
