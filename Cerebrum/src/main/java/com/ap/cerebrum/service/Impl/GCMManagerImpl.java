package com.ap.cerebrum.service.Impl;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.ap.cerebrum.util.GlobalConstants;
import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.GCMManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by aniket on 3/14/14.
 */
public class GCMManagerImpl implements GCMManager {

    private GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    String regid;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    static final String TAG = "com.ap.cerebrum.GCMManager";

    @Override
    public boolean checkPlayServices(Context context, Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }


    /**
     * Not in use.
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    @Override
    public void registerInBackground(final Context context) {
        ChatManager chatManager = new ChatManagerImpl();
        regid = chatManager.getRegistrationId(context);
        if(regid==null || regid.trim().length()<=0)
        {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    try {
                        if (gcm == null) {
                            gcm = GoogleCloudMessaging.getInstance(context);
                        }
                        regid = gcm.register(GlobalConstants.SENDER_ID);
                        msg = "Device registered, registration ID=" + regid;

                        // You should send the registration ID to your server over HTTP, so it
                        // can use GCM/HTTP or CCS to send messages to your app.
                        sendRegistrationIdToBackend();

                        // For this demo: we don't need to send it because the device will send
                        // upstream messages to a server that echo back the message using the
                        // 'from' address in the message.

                        // Persist the regID - no need to register again.
                        ChatManager chatManager = new ChatManagerImpl();
                        chatManager.storeRegistrationId(context, regid);
                    } catch (IOException ex) {
                        msg = "Error :" + ex.getMessage();
                        // If there is an error, don't just keep trying to register.
                        // Require the user to click a button again, or perform
                        // exponential back-off.
                    }
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                   // mDisplay.append(msg + "\n");
                }
            }.execute(null, null, null);

        }
    }


    /**
     * Not in Use.
     * @param data
     * @param context
     * @throws IOException
     */
    @Override
    public void sendMessage(Bundle data,Context context) throws IOException{
        String id = Integer.toString(msgId.incrementAndGet());
        gcm = GoogleCloudMessaging.getInstance(context);
        gcm.send(GlobalConstants.SENDER_ID + "@gcm.googleapis.com", id, data);
    }

    /**
     * Not in Use.
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    public void sendRegistrationIdToBackend() {
        // Your implementation here.
    }
}
