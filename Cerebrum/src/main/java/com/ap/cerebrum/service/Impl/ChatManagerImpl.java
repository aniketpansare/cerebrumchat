package com.ap.cerebrum.service.Impl;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.ap.cerebrum.database.ChatDataSource;
import com.ap.cerebrum.domain.Contact;
import com.ap.cerebrum.security.Impl.SecurityManagerImpl;
import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.ContactManager;
import com.ap.cerebrum.tasks.RegisterDeviceTask;
import com.ap.cerebrum.tasks.SendMessageTask;
import com.ap.cerebrum.util.GlobalConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by aniket on 3/14/14.
 * @author Aniket Pansare
 */
public class ChatManagerImpl implements ChatManager {

    static final String TAG = "com.ap.cerebrum.ChatManager";
    private String regID= null;
    List<Contact> contactList;

    @Override
    public void activateApp(Context context, String name, String email)
    {
        com.ap.cerebrum.security.SecurityManager securityManager = new SecurityManagerImpl();
        // Check if user filled the form
        if(name.trim().length() > 0 && email.trim().length() > 0){
            if(securityManager.isEmail(email))
            {
                //Register user with GCM server and fetch registration id in call back method.
                Intent registerIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
                registerIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
                registerIntent.putExtra("sender","269142109448");
                storeName(context,name);
                storeEmail(context,email);
                context.startService(registerIntent);
            }
            else
            {
                Toast.makeText(context,"Enter valid Email", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(context,"Enter valid Name and Email", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handleRegistration(Context context, Intent intent) {

        String registrationId = intent.getStringExtra("registration_id");

        if (intent.getStringExtra("error") != null) {
            Log.d(TAG, "registration failed");
            String error = intent.getStringExtra("error");
            if(error == "SERVICE_NOT_AVAILABLE"){
                Log.d(TAG, "SERVICE_NOT_AVAILABLE");
            }else if(error == "ACCOUNT_MISSING"){
                Log.d(TAG, "ACCOUNT_MISSING");
            }else if(error == "AUTHENTICATION_FAILED"){
                Log.d(TAG, "AUTHENTICATION_FAILED");
            }else if(error == "TOO_MANY_REGISTRATIONS"){
                Log.d(TAG, "TOO_MANY_REGISTRATIONS");
            }else if(error == "INVALID_SENDER"){
                Log.d(TAG, "INVALID_SENDER");
            }else if(error == "PHONE_REGISTRATION_ERROR"){
                Log.d(TAG, "PHONE_REGISTRATION_ERROR");
            }
        } else if (intent.getStringExtra("unregistered") != null) {
            Log.d(TAG, "unregistered");
            Toast.makeText(context, "Application unregistered successfully", Toast.LENGTH_SHORT).show();

        } else if (registrationId != null) {
            String name =  getNameFromPreferences(context);
            String email = getEmailFromPreferences(context);

            storeRegistrationId(context,registrationId);
            Toast.makeText(context,"Registration Id:"+registrationId,Toast.LENGTH_SHORT).show();
            regID = registrationId;

            new RegisterDeviceTask(context).execute(regID,name,email);
           // new FetchContactsTask().execute(regID,name,email);
        }
    }

    @Override
    public void handleMessage(Context context, Intent intent) {
        String a = intent.getAction();
        if (a.equals("com.google.android.c2dm.intent.RECEIVE"))
        {
            String fromRegistrationId = intent.getStringExtra("fromRegistrationId");
            String toRegistrationId = intent.getStringExtra("toRegistrationId");
            String chatText = intent.getStringExtra("chatText");
            String fromName = intent.getStringExtra("fromName");
            String toName = intent.getStringExtra("toName");
            if(chatText!=null)
            {
                String payload = chatText;
                //Toast.makeText(context, payload, Toast.LENGTH_SHORT).show();
                ChatDataSource dataSource= new ChatDataSource(context);
                dataSource.createChatData(fromName,payload,1);
            }
            else{
                contactList = new ArrayList<Contact>();
                String buffer;
                for(int i=0;(buffer=intent.getStringExtra(String.valueOf(i)))!= null;i++)
                {
                    String bufArray[] = buffer.replaceAll("[\"\\[\\]]","").split(",");
                    if(bufArray.length >= 3) {
                        String name = bufArray[1];
                        String email = bufArray[2];
                        String registrationId = bufArray[0];
                        Contact contact = ContactManager.createContact(name, email, registrationId);
                        contactList.add(contact);
                    }
                }
                storeContactList(context,contactList);
            }
        }
    }

    @Override
    public String sendMessage(Contact toContact, String msg,Context context) {
                String result=null;

                 /*
                try {
                    Bundle data = new Bundle();
                    data.putString("sourceRegistrationId",getRegistrationId(context));
                    data.putString("targetRegistrationId",toContact.getRegistrationId());
                    data.putString("chatText", msg);
                    data.putString("my_action", "com.google.android.gcm.demo.app.ECHO_NOW");
                    GCMManager gcmManager = new GCMManagerImpl();
                    gcmManager.sendMessage(data,context);
                    result=msg;

                } catch (IOException ex) {
                    result = "Error :" + ex.getMessage();
                }
                */

                 String fromRegId=getRegistrationId(context);
                 String toRegId=toContact.getRegistrationId();
                 String fromEmail= getEmailFromPreferences(context);
                 String toEmail=toContact.getEmail();
                 String toName=toContact.getName();
                 String fromName=getNameFromPreferences(context);
                 new SendMessageTask().execute(toRegId,fromRegId,msg,toEmail,fromEmail,toName,fromName);
                 result=msg;

                return result;
    }

    @Override
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public Set<String> getContactNames(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.CONTACT_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getAll().keySet();
    }

    @Override
    public void storeContactList(Context context, List<Contact> contacts) {
        SharedPreferences.Editor editor =
                context.getSharedPreferences(GlobalConstants.CONTACT_FILE_NAME, Context.MODE_PRIVATE).edit();
        for(Contact contact : contacts)
        {
            //Email Not handled. Need to pass email while creating contact object.
            if(!getNameFromPreferences(context).contentEquals(contact.getName()))
                    editor.putString(contact.getName(),contact.getRegistrationId());
        }
        editor.commit();
    }

    @Override
    public void storeName(Context context, String name) {
        Log.d(TAG, name);
        SharedPreferences.Editor editor =
                context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(GlobalConstants.NAME, name);
        Log.i(TAG, "Saving user name" + name);
        editor.commit();
    }

    @Override
    public void storeEmail(Context context, String email) {
        Log.d(TAG, email);
        SharedPreferences.Editor editor =
                context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(GlobalConstants.EMAIL, email);
        Log.i(TAG, "Saving user email" + email);
        editor.commit();
    }


    @Override
    public void storeRegistrationId(Context context, String regId) {
        Log.d(TAG, regId);
        SharedPreferences.Editor editor =
                context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(GlobalConstants.REGISTRATION_ID, regId);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        editor.putInt(GlobalConstants.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }



    @Override
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public String getRegistrationId(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        String registrationId = prefs.getString(GlobalConstants.REGISTRATION_ID, "");
        if (registrationId== null || registrationId.trim().length() <= 0) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(GlobalConstants.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    @Override
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public String getContactRegId(Context context, String contactName) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.CONTACT_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getString(contactName,"");
    }

    @Override
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public String getNameFromPreferences(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        String name = prefs.getString(GlobalConstants.NAME,"");
        if (name == null || name.trim().length() <= 0) {
            Log.i(TAG, "Name not found.");
            return "";
        }
        return name;
    }

    @Override
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public String getEmailFromPreferences(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        String email = prefs.getString(GlobalConstants.EMAIL,"");
        if (email==null || email.trim().length() <= 0) {
            Log.i(TAG, "Email not found.");
            return "";
        }
        return email;
    }


    @Override
    public int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    @Override
    public boolean isLoggedIn(Context context) {
        final SharedPreferences prefs = context.getSharedPreferences(GlobalConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        String name = prefs.getString(GlobalConstants.NAME,"");
        String email = prefs.getString(GlobalConstants.EMAIL,"");
        if (email == null || name == null || name.trim().length() <= 0 || email.trim().length() <= 0) {
            Log.i(TAG, "User Not Logged in");
            return false;
        }
        return true;
    }
}
