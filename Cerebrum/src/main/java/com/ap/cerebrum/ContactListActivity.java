package com.ap.cerebrum;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.Impl.ChatManagerImpl;
import com.ap.cerebrum.tasks.FetchContactsTask;
import com.ap.cerebrum.util.ChatArrayAdapter;


/**
 * Contact List Activity.
 * Displays all contacts of the user.
 * @author Aniket Pansare
 */
public class ContactListActivity extends ListActivity {

    /**
     * Chat manager for handling sending and receiving of messages.
     */
    ChatManager chatManager = new ChatManagerImpl();

    ChatArrayAdapter listAdapter;

    /**
     * Progress bar
     */
    private ProgressDialog progress;

    /************************************************************************************
     *  Activity Methods.
     ***********************************************************************************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        setContentView(R.layout.activity_contactlist); //causes error for List view
        */

        ListView listView = getListView();
        listView.setTextFilterEnabled(true);

        Intent intent = getIntent();
        String regId = intent.getStringExtra("regId");
        String name = intent.getStringExtra("name");
        String email = intent.getStringExtra("email");
        new FetchContactsTask(this).execute(regId,name,email);
        progress= new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setMessage("Fetching Contacts...");
        progress.show();


        //Initialize array Adapter
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_contactlist,
                chatManager.getContactNames(getApplicationContext()).toArray(new String[0])));

        //Set on click Listener on the List view.
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String name=((TextView) view).getText().toString();
                Intent i = new Intent(getApplicationContext(), ChatWindowActivity.class);
                i.putExtra("name", name);
                startActivity(i);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        //Initialize Custom Array Adapter with contact list.
        List<String> list = new ArrayList<String>();
        list.addAll(chatManager.getContactNames(getApplicationContext()));
        ContactListAdapter listAdapter = new ContactListAdapter(getApplicationContext(),list);
        setListAdapter(listAdapter);
        */

        //Initialize array adapter on Resume.
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_contactlist,
                chatManager.getContactNames(getApplicationContext()).toArray(new String[0])));

    }


    public void fetchContactsCallback()
    {
        progress.dismiss();
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_contactlist,
                chatManager.getContactNames(getApplicationContext()).toArray(new String[0])));
    }

}
