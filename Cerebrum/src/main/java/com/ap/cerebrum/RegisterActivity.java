package com.ap.cerebrum;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ap.cerebrum.service.ChatManager;
import com.ap.cerebrum.service.Impl.ChatManagerImpl;
import com.ap.cerebrum.util.ConnectionDetector;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;

import static com.ap.cerebrum.util.GlobalConstants.SENDER_ID;
import static com.ap.cerebrum.util.GlobalConstants.SERVER_URL;


/**
 * Registers the User and acts a Login Screen.
 * This activity will be visible only once, when app is newly installed.
 * @author Aniket Pansare
 */
public class RegisterActivity extends Activity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {


    public static final String TAG = "com.ap.cerebrum.RegisterActivity";
    /**
     * To check internet connection.
     */
    ConnectionDetector cd;

    /**
     * Chat Manager for managing sending and receiving of messages.
     */
    ChatManager chatManager;

    /**
     * Login Screen Field : Name
     */
    EditText txtName;

    /**
     * Login Screen Field : Email
     */
    EditText txtEmail;

    /**
     * Activate,Deactivate and Login Buttons.
     */
    Button btnActivate,btnDeactivate,btnLogin;
    SignInButton  googleBtnSignIn;

    /***************************************************************************************
     *  Activity methods.
     ***************************************************************************************/

    /**
     * On create method for RegisterActivity.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*//Facebook Integration
        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);

        Request request = Request.newGraphPathRequest(null, "/4", new Request.Callback() {
            @Override
            public void onCompleted(Response response) {
                if(response.getError() != null) {
                    Log.i("MainActivity", String.format("Error making request: %s", response.getError()));
                } else {
                    GraphUser user = response.getGraphObjectAs(GraphUser.class);
                    Log.i("MainActivity", String.format("Name: %s", user.getName()));
                }
            }
        });
        request.executeAsync();
        */
        mContext=getApplicationContext();

        cd = new ConnectionDetector(getApplicationContext());
        chatManager = new ChatManagerImpl();

        //check for internet Connection
        if (!cd.isConnectingToInternet()) {
            Log.i("cerebrum", "Please connect to working Internet connection");
            return;
        }

        // Check if GCM configuration is set
        if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0
                || SENDER_ID.length() == 0) {
              Log.i("cerebrum", "Please set your Server URL and GCM Sender ID");
            return;
        }

        /*
        //check if user is logged in
        if(chatManager.isLoggedIn(getApplicationContext()))
        {
            Context context = getApplicationContext();
            String name1 =  chatManager.getNameFromPreferences(context);
            String email = chatManager.getEmailFromPreferences(context);
            String regID = chatManager.getRegistrationId(context);
            //need to remove this.. (heavy task)
            new FetchContactsTask().execute(regID,name1,email);

            //Invoke next activity
            Intent i = new Intent(getApplicationContext(), ContactListActivity.class);
            startActivity(i);
            finish();
        }*/


        //If user not logged in set Content View and initialize fields.
        setContentView(R.layout.activity_register);
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnActivate= (Button) findViewById(R.id.btnActivate);
        btnDeactivate= (Button) findViewById(R.id.btnDeactivate);

        /*********************************************************************
         *  Sign In with Google Account
         *********************************************************************/

        googleBtnSignIn = (SignInButton) findViewById(R.id.sign_in_button);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, null)
                .addScope(Plus.SCOPE_PLUS_LOGIN)//.SCOPE_PLUS_LOGIN)
                .build();

        /*********************************************************************
         *  Listeners for Activity Buttons
         *********************************************************************/

        //Set on click listener for Login button.
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String name = txtName.getText().toString();
                String email = txtEmail.getText().toString();

                Context context = getApplicationContext();
                if(name.trim().length() > 0 && email.trim().length() > 0 &&
                        chatManager.getNameFromPreferences(context).contentEquals(name) &&
                        chatManager.getEmailFromPreferences(context).contentEquals(email))
                {
                    //Start Next Activity if login successful.
                    startNextActivity("",name,email);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Enter valid Name and Email or Activate User", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //Set on click listener for Activate Button
        btnActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = txtName.getText().toString();
                String email = txtEmail.getText().toString();
                chatManager.activateApp(getApplicationContext(),name,email);
            }
        });

        // Set on click listener for deactivate button.
        btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent unregiterIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
                unregiterIntent.putExtra("app", PendingIntent.getBroadcast(view.getContext(),0,new Intent(),0));
                startService(unregiterIntent);
                /**
                 * Handle server database on un-register.
                 */
            }
        });
        //Set invisible temporary.
        btnDeactivate.setVisibility(View.INVISIBLE);


        //Google+ Button
        googleBtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (view.getId() == R.id.sign_in_button
                        && !mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    resolveSignInError();
                }
            }
        });

    }





    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* A flag indicating that a PendingIntent is in progress and prevents
 * us from starting further intents.
  */
    private boolean mIntentInProgress;

    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /* Track whether the sign-in button has been clicked so that we know to resolve
 * all issues preventing sign-in without waiting.
 */
    private boolean mSignInClicked;

    /* Store the connection result from onConnectionFailed callbacks so that we can
     * resolve them when the user clicks sign-in.
     */
    private ConnectionResult mConnectionResult;

    /**
     * Progress bar
     */
    private static ProgressDialog progress;
    private static String loginUserName;
    private static String loginUserEmail;
    private static Context mContext;



    /**
     * Friends List
     */
    PersonBuffer friendsBuffer;

    protected void onStart() {
        super.onStart();
         mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

    }

    /* A helper method to resolve the current ConnectionResult error. */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
               // startIntentSenderForResult(mConnectionResult.getIntentSender(),RC_SIGN_IN, null, 0, 0, 0);
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!mIntentInProgress) {
            // Store the ConnectionResult so that we can use it later when the user clicks
            // 'sign-in'.
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        // We've resolved any connection errors.  mGoogleApiClient can be used to
        // access Google APIs on behalf of the user.
        mSignInClicked = false;


        loginUserEmail = Plus.AccountApi.getAccountName(mGoogleApiClient);
        loginUserName = loginUserEmail; //Set Default Name = Email.

        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person mePerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            loginUserName=mePerson.getDisplayName();
            System.out.println("ID:\t" + mePerson.getId());
            System.out.println("Image URL:\t" + mePerson.getImage().getUrl());
            System.out.println("Profile URL:\t" + mePerson.getUrl());

        }

        Toast.makeText(this, "User is connected! : "+loginUserEmail, Toast.LENGTH_LONG).show();

        //Register APP get registraion id in call back
        chatManager.activateApp(getApplicationContext(),loginUserName,loginUserEmail);

        //fetch contacts
        //new FetchContactsTask().execute(regID,name1,email);
        progress= new ProgressDialog(this);
        progress.setIndeterminate(true);
        progress.setMessage("Signing In...");
        progress.show();

    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onResult(People.LoadPeopleResult peopleData) {
        if (peopleData.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
            friendsBuffer = peopleData.getPersonBuffer();
            try {
                int count = friendsBuffer.getCount();
                for (int i = 0; i < count; i++) {
                    Log.d(TAG, "Display name: " + friendsBuffer.get(i).getDisplayName());
                    Log.d(TAG, "Display name: " + friendsBuffer.get(i).getName());
                }
            } finally {
                friendsBuffer.close();
            }
        } else {
            Log.e(TAG, "Error requesting visible circles: " + peopleData.getStatus());
        }
    }

    public static void startNextActivity(String registrationId, String name, String email)
    {
        //Start Next Activity if login successful.
        Intent i = new Intent(getStaticAppContext(), ContactListActivity.class);
        i.putExtra("regId", registrationId);
        i.putExtra("name", name);
        i.putExtra("email", email);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
        //finish();
    }

    public static void registerDeviceCallback(String registrationId)
    {
        progress.dismiss();
        startNextActivity(registrationId, loginUserName, loginUserEmail);
    }

    public static Context getStaticAppContext(){
        return mContext;
    }

}
