package com.ap.cerebrum.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by aniket on 3/17/14.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION=3;
    public static final String DATABASE_NAME="CHAT.db";
    public static final String COLUMN_ID="_id";
    public static final String COLUMN_NAME="name";
    public static final String COLUMN_CHATTEXT="chattext";
    public static final String COLUMN_DIRECTION="direction";
    public static final String TABLE_CHAT_HIST="CHAT_HISTORY";
    private static final String CREATE_TABLE_CHAT_HIST=
            "CREATE TABLE "+TABLE_CHAT_HIST+" ("+
                    COLUMN_ID +" integer primary key autoincrement, " +
                    COLUMN_NAME +" TEXT not null, "+
                    COLUMN_CHATTEXT +" TEXT, " +
                    COLUMN_DIRECTION + " integer );";

    public MySQLiteHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
       sqLiteDatabase.execSQL(CREATE_TABLE_CHAT_HIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // will destroy old data
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CHAT_HIST);
        onCreate(db);
    }
}
