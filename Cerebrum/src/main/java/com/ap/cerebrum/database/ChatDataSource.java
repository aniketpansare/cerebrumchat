package com.ap.cerebrum.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ap.cerebrum.domain.ChatData;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * ChatDataSource class to read and write chat data to database.
 * @author Aniket Pansare
 */
public class ChatDataSource {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] projection={MySQLiteHelper.COLUMN_ID,MySQLiteHelper.COLUMN_NAME,MySQLiteHelper.COLUMN_CHATTEXT,MySQLiteHelper.COLUMN_DIRECTION};

    /**
     * constructor.
     * @param context
     */
    public ChatDataSource(Context context)
    {
        dbHelper=new MySQLiteHelper(context);
    }

    /**
     * Open Call.
     */
    public void open()
    {
        database=dbHelper.getWritableDatabase();
    }

    /**
     * close dbHelper call.
     */
    public void close()
    {
        dbHelper.close();
    }

    /**
     * Create Chat entry in database.
     * one entry for each sent or received message.
     * @param name
     * @param chattext
     * @param direction
     * @return
     */
    public long createChatData(String name, String chattext,int direction)
    {
        database=dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_NAME,name);
        values.put(MySQLiteHelper.COLUMN_CHATTEXT,chattext);
        values.put(MySQLiteHelper.COLUMN_DIRECTION,direction);
        String query="INSERT into "+MySQLiteHelper.TABLE_CHAT_HIST+" (" +
                MySQLiteHelper.COLUMN_NAME+", "+MySQLiteHelper.COLUMN_CHATTEXT+", " +
                MySQLiteHelper.COLUMN_DIRECTION+" ) VALUES "+"('"+name+"', '" +
                chattext+"', "+direction+");";

        database.execSQL(query);
        database.close();
        return 0;
    }

    /**
     * Fetch Chat history based on name.
     * @param name
     * @return
     */
    public List<String> getChatHistory(String name)
    {
        database=dbHelper.getReadableDatabase();
        List<String> hist = new ArrayList<String>();
        String selection=MySQLiteHelper.COLUMN_NAME +" = '"+name+"'";
        String sortOrder = MySQLiteHelper.COLUMN_ID + " DESC";
        Cursor cursor = database.query(MySQLiteHelper.TABLE_CHAT_HIST,projection,selection,null,null,null,sortOrder,"50");

        if(cursor!=null)
        {
            cursor.moveToFirst();
            while(!cursor.isAfterLast())
            {
                if(cursor.getInt(3)==1)
                {
                hist.add(0,cursor.getString(1)+":"+cursor.getInt(3)+":"+cursor.getString(2));
                }
                else
                {
                    hist.add(0,cursor.getInt(3)+":"+cursor.getString(2));
                }
                cursor.moveToNext();
            }
        }
        database.close();
        return hist;
    }
}
