package com.ap.cerebrum.security;

import com.ap.cerebrum.security.Impl.SecurityManagerImpl;

/**
 * Created by aniket on 3/14/14.
 */
public abstract class SecurityManager {

    /**
     * Singleton Object.
    */
    /*
    private static SecurityManager securityManager = null;
    public static synchronized SecurityManager getInstance()
    {
        if(securityManager == null)
            securityManager=new SecurityManagerImpl();

        return securityManager;
    }
    */

    /**
     * Create separate instance of security Manager for each thread and avoids synchronization.
     */
    private static final ThreadLocal<SecurityManagerImpl> secman = new ThreadLocal<SecurityManagerImpl>(){
        @Override
        protected synchronized SecurityManagerImpl initialValue() {
            return new SecurityManagerImpl();
        }
    };

    /**
     * Factory Method get Security Manager object.
     * @return
     */
    public static SecurityManager get() {
        return secman.get();
    }

    /**********************************************************************
     *  Validation Functions.
     *********************************************************************/

   /**
     * Determines if the input string is a valid username.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isUsername(String input) throws NullPointerException;

    /**
     * Determines if the input string is a valid email address.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isEmail(String input) throws NullPointerException;

    /**
     * Determines if the input string is a valid password.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isPassword(String input) throws NullPointerException;

    /**
     * Determines if the input string is a valid phone number.
     * @param input The string to test.
     * @return True if the input is valid.
     * @throws NullPointerException Thrown if input is NULL.
     */
    public abstract boolean isPhoneNumber(String input) throws NullPointerException;

}
