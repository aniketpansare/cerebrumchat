package com.ap.cerebrum.security.Impl;

import com.ap.cerebrum.security.*;
import com.ap.cerebrum.util.GlobalConstants;
import com.ap.cerebrum.security.SecurityManager;

/**
 * Created by aniket on 3/14/14.
 * @author Aniket Pansare
 */
public class SecurityManagerImpl extends SecurityManager {

    /// Email address regex pattern
    /// (from http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/)
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /// Pattern to match valid usernames against.
    private static final String USERNAME_PATTERN =
            "^[a-zA-Z][a-zA-Z0-9_]*$";

    /// Pattern to match valid phone numbers against.
    private static final String PHONENUMBER_PATTERN =
            "^(\\d{3}-\\d{3}-\\d{4})"
                    + "|(\\(\\d{3}\\)\\s*\\d{3}-\\d{4})"
                    + "|(\\d{3}-\\d{7})"
                    + "|(\\d{10})$";


    /*********************************************************************
     *  Validation functions
     **********************************************************************/



    // Determines if the input string is a valid username.
    @Override
    public boolean isUsername(String input) throws NullPointerException {
        if (input == null) {
            NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            //_log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() <= GlobalConstants.USERNAME_MAX_LENGTH
                && input.length() >= GlobalConstants.USERNAME_MIN_LENGTH
                && input.matches(USERNAME_PATTERN);
    }

    // Determines if the input string is a valid email address.
    @Override
    public boolean isEmail(String input) throws NullPointerException {
        if (input == null) {
            NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            //_log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() <= GlobalConstants.EMAIL_MAX_LENGTH && input.matches(EMAIL_PATTERN);
    }

    // Determines if the input string is a valid password.
    @Override
    public boolean isPassword(String input) throws NullPointerException {
        if (input == null) {
            NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            //_log.catching(Priority.WARN, e);
            throw e;
        }
        return input.length() >= GlobalConstants.PASSWORD_MIN_LENGTH && input.length() <= GlobalConstants.PASSWORD_MAX_LENGTH;
    }

    // Determines if the input string is a valid phone number.
    @Override
    public boolean isPhoneNumber(String input) throws NullPointerException {
        if (input == null) {
            NullPointerException e = new  NullPointerException("Illegal attempt to validate a null string.");
            //_log.catching(Priority.WARN, e);
            throw e;
        }
        return input.matches(PHONENUMBER_PATTERN);
    }


}
