# Andriod App (Project - Cerebrum)

1. install git on your computer (http://git-scm.com/download)
2. open git bash shell
3. Try cloning the repository with below command.
     git clone git@bitbucket.org:aniketpansare/cerebrumchat.git
4. If permission denied error, enter below commands one by one:
     1. ssh-keygen
          press Enter Enter.. (default settings)
     2. cat ~/.ssh/id_rsa.pub
     3. copy the entire output of cat- it is a RSA key
     4. Go to bitbucket--> Right hand corner--> click your profile --> go to manage accounts 
     5. Left side you will see SSH keys option. Click it.
     6. Click Add SSH key, and copy paste above key. (give any default name)
     7. Try command : git clone git@bitbucket.org:aniketpansare/cerebrumchat.git
5. Go inside cerebum directory & Execute command to resolve dependencies: gradlew
6. Connect your phone with USB debugging on & execute: gradlew installDebug 